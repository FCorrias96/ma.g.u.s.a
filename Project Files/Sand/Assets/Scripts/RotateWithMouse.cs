﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWithMouse : MonoBehaviour
{
	public float rotationSpeed = 10f;
	float friction = 0.5f;
	float xDeg;
	float yDeg;
	Quaternion fromRotation;
	Quaternion toRotation;

	void OnMouseDrag()
	{
		xDeg -= Input.GetAxis("Mouse X") * rotationSpeed * friction;
		yDeg += Input.GetAxis("Mouse Y") * rotationSpeed * friction;
		fromRotation = transform.rotation;
		toRotation = Quaternion.Euler(yDeg, xDeg, 0);
		transform.rotation = Quaternion.Lerp(fromRotation, toRotation, Time.deltaTime * rotationSpeed);
	}
}
