﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop : MonoBehaviour
{
    public bool isMouseDrag=false;
    public GameObject[] cogs;
    GameObject target = null;

    // Update is called once per frame
    void Update()
    {
        Vector3 offset = new Vector3(0, 0, 0), screenPosition;

        if (Input.GetMouseButtonDown(0)) {
            //find mouse target
            foreach (GameObject cog in cogs) {
                if (cog.GetComponent<Rotate>().isMovable==true) {
                    Vector3 m_point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    m_point = new Vector3(m_point.x, m_point.y, 0);
                    if (cog.GetComponent<Collider>().bounds.Contains(m_point)) {
                        isMouseDrag = true;
                        target = cog;
                    }
                    if (target != null) {
                        //Convert world position to screen position.
                        screenPosition = Camera.main.WorldToScreenPoint(target.transform.position);
                        offset = target.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
                    }
                }
            }
        }

        if (Input.GetMouseButtonUp(0)) {
            if (target != null) {
                target.GetComponent<Rotate>().moving = false;
                isMouseDrag = false;
                target = null;
            }
        }

        if (isMouseDrag && target!=null) {
            target.GetComponent<Rotate>().moving = true;
            //track mouse position.
            Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);

            //convert screen position to world position with offset changes.
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offset;

            //It will update target gameobject's current postion.
            target.transform.position = new Vector3(currentPosition.x, currentPosition.y, 0);
        }
    }

}
