﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPosition : MonoBehaviour
{
    public Sprite correctSprite;
    public bool rotated;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool isCorrect() {
        SpriteRenderer s = GetComponent<SpriteRenderer>();
        return s.flipY==rotated && s.sprite==correctSprite;
    }
}
