﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swap : MonoBehaviour
{
    public GameObject[] sprites;
    public GameObject highlight;
    GameObject selected=null;
    bool is_selected;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject target=null;
        if (Input.GetMouseButtonDown(0)) {
            Vector3 m_point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            m_point = new Vector3(m_point.x, m_point.y, 0);
            foreach(GameObject sprite in sprites) {
                if (sprite.GetComponent<Collider>().bounds.Contains(m_point)) {
                    target = sprite;
                }
            }
            if (target != null) {
                if (is_selected == true) {
                    if (selected != target) {
                        Sprite s= selected.GetComponent<SpriteRenderer>().sprite;
                        selected.GetComponent<SpriteRenderer>().sprite = target.GetComponent<SpriteRenderer>().sprite;
                        target.GetComponent<SpriteRenderer>().sprite = s;

                        bool flipped= selected.GetComponent<SpriteRenderer>().flipY;
                        selected.GetComponent<SpriteRenderer>().flipY = target.GetComponent<SpriteRenderer>().flipY;
                        target.GetComponent<SpriteRenderer>().flipY = s;
                    }
                    highlight.SetActive(false);
                    selected = null;
                    is_selected = false;
                } else {
                    highlight.GetComponent<Transform>().position = new Vector3(target.GetComponent<Transform>().position.x, target.GetComponent<Transform>().position.y, -0.5f);
                    highlight.SetActive(true);
                    selected = target;
                    is_selected = true;
                }
            }
        }
        if (Input.GetMouseButtonDown(1)) {
            Vector3 m_point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            m_point = new Vector3(m_point.x, m_point.y, 0);
            foreach (GameObject sprite in sprites) {
                if (sprite.GetComponent<Collider>().bounds.Contains(m_point)) {
                    target = sprite;
                }
            }
            if (target != null) {
                target.GetComponent<SpriteRenderer>().flipY=!target.GetComponent<SpriteRenderer>().flipY;
            }
        }
    }

}
