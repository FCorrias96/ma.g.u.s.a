﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ThirdController : MonoBehaviour
{
    int completed = 0;
    public GameObject[] sections;
    public Button restart, endButton, restartMessage, endButton2;
    public GameObject endMessage;

    // Start is called before the first frame update
    void Start() {
        foreach (GameObject section in sections) {
            if (section.GetComponent<CheckPosition>().isCorrect() == true) {
                completed++;
            }
        }
        restart.onClick.AddListener(restartPuzzle);
        restartMessage.onClick.AddListener(restartPuzzle);
        endButton.onClick.AddListener(Quit);
        endButton2.onClick.AddListener(Quit);
    }

    // Update is called once per frame
    void Update() {
        completed = 0;
        foreach (GameObject section in sections) {
            if (section.GetComponent<CheckPosition>().isCorrect() == true) {
                completed++;
            }
        }
        if (completed >= sections.Length) {
            endMessage.SetActive(true);
        }
    }

    void restartPuzzle() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void Quit() {
        Application.Quit();
    }
}
