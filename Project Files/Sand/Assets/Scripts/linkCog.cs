﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class linkCog : MonoBehaviour
{
    public GameObject correctCog, correctCog2=null;
    public bool correct;
    public GameObject precedent = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (correctCog.GetComponent<Rotate>().correct == true)
        {
            Vector3 position = new Vector3(transform.position.x, transform.position.y, 0);
            correctCog.gameObject.transform.position = position;
            correctCog.GetComponent<Rotate>().linked = true;
        }*/
    }

    private void OnTriggerEnter(Collider other)
    {

        Vector3 position = new Vector3(transform.position.x, transform.position.y, 0);
        other.gameObject.transform.position = position;
    }

    private void OnTriggerStay(Collider other) {
        //other.GetComponent<Rotate>().linked = true;
        //other.GetComponent<Rotate>().linkedTo = this;

        if (Input.GetMouseButton(0)==false){ 
            Vector3 position = new Vector3(transform.position.x, transform.position.y, 0);
            other.gameObject.transform.position = position; 
            other.GetComponent<Rotate>().linked = true;
            other.GetComponent<Rotate>().linkedTo = this;
            if (other.gameObject == correctCog) {
                correct = true;
                correctCog.GetComponent<Rotate>().correct = true;
                //correctCog.GetComponent<Rotate>().moving = false;
                //correctCog.GetComponent<DragAndDrop>().movable = false; 
                //correctCog.GetComponent<DragAndDrop>().isMouseDrag = false; 
            }
            if (other.gameObject == correctCog2) {
                correct = true;
                correctCog2.GetComponent<Rotate>().correct = true;
                //correctCog.GetComponent<Rotate>().moving = false;
                //correctCog.GetComponent<DragAndDrop>().movable = false; 
                //correctCog.GetComponent<DragAndDrop>().isMouseDrag = false; 
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //if (other.gameObject != correctCog)
        {
            other.GetComponent<Rotate>().linked = false;
            other.GetComponent<Rotate>().linkedTo = null;
        }
        if (other.gameObject == correctCog) {
            correct = false;
            correctCog.GetComponent<Rotate>().correct = false;
        }
        if (correctCog2!=null && correctCog2 == other.gameObject) {
            correct = false;
            correctCog2.GetComponent<Rotate>().correct = false;
        }
    }

    public bool isCorrect() {
        if (precedent == null) {
            return true;
        } else {
            return precedent.GetComponent<linkCog>().correct && precedent.GetComponent<linkCog>().isCorrect();
        }
    }
}
