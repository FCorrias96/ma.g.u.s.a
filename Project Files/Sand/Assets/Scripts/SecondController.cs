﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SecondController : MonoBehaviour
{
    int total = 0;
    public GameObject[] switches;
    public Button restart, nextLevel, restartMessage, endButton;
    public GameObject endMessage, hint1, hint2, hint3, textHint1, textHint2, textHint3;

    // Start is called before the first frame update
    void Start() {
        foreach (GameObject m_switch in switches) {
            total += m_switch.GetComponent<Switch>().value;
        }
        restart.onClick.AddListener(restartPuzzle);
        restartMessage.onClick.AddListener(restartPuzzle);
        nextLevel.onClick.AddListener(LoadPuzzle);
        hint1.GetComponent<Button>().onClick.AddListener(delegate { this.showHint(hint1, textHint1); });
        hint2.GetComponent<Button>().onClick.AddListener(delegate { this.showHint(hint2, textHint2); });
        hint3.GetComponent<Button>().onClick.AddListener(delegate { this.showHint(hint3, textHint3); });
        endButton.onClick.AddListener(Quit);
    }

// Update is called once per frame
    void Update(){

        if (Input.GetMouseButtonUp(0)) {
            total = 0;
            foreach (GameObject m_switch in switches) {
                total += m_switch.GetComponent<Switch>().value;
            }
        }

        if (total == 7) {
            foreach (GameObject m_switch in switches) {
                m_switch.GetComponent<Switch>().clickable=false;
            }
            endMessage.SetActive(true);
        }
    }


    void restartPuzzle() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void LoadPuzzle() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void showHint(GameObject button, GameObject text) {
        button.SetActive(false);
        text.SetActive(true);
    }

    void Quit() {
        Application.Quit();
    }
}