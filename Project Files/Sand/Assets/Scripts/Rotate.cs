﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    public float speed = 10;
    public Vector3 direction;
    public bool moving = false, correct=false, linked, isMovable;
    public linkCog linkedTo=null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (linkedTo != null) {
            if (linkedTo.isCorrect()) {
                if (!moving && linked) {
                    if (correct) {
                        transform.Rotate(direction * speed * Time.deltaTime);
                    } else {
                        transform.Rotate(direction * speed / 10 * Time.deltaTime);
                    }
                }
            };
        }
    }
}
