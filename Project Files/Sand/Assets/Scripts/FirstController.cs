﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FirstController : MonoBehaviour
{
    int completed = 0;
    public GameObject[] cogs;
    public Button restart, nextLevel, restartMessage, endButton;
    public GameObject endMessage;

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject cog in cogs)
        {
            if (cog.GetComponent<Rotate>().correct == true)
            {
                completed++;
            }
        }
        restart.onClick.AddListener(restartPuzzle);
        restartMessage.onClick.AddListener(restartPuzzle);
        nextLevel.onClick.AddListener(LoadPuzzle);
        endButton.onClick.AddListener(Quit);
    }

    // Update is called once per frame
    void Update()
    {
        completed = 0;
        foreach (GameObject cog in cogs)
        {
            if (cog.GetComponent<Rotate>().correct == true)
            {
                completed++;
            }
        }
        if (completed >= cogs.Length)
        {
            endPuzzle();
        }
    }

    void endPuzzle()
    {
        endMessage.SetActive(true);
    }

    void restartPuzzle()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void LoadPuzzle()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void Quit() {
        Application.Quit();
    }
}
