﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Switch : MonoBehaviour
{
    public GameObject lever;
    bool active = false;
    public int value, increment;
    public Text show_value;
    public Light led;
    public bool clickable = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnMouseDown() {
        float movement;
        if (clickable) {
            if (active) {

                //sposta leva
                movement = -30f;
                value -= increment;
                led.color = Color.red;
            } else {
                movement = 30f;
                value += increment;
                led.color = Color.green;
            }


            //sposta leva
            lever.transform.position = new Vector3(lever.transform.position.x + movement, lever.transform.position.y, lever.transform.position.z);

            //cambia testo
            if (value > 0) {
                show_value.text = "+" + value;
            } else {
                show_value.text = "" + value;
            }

            active = !active;
        }

    }
}
